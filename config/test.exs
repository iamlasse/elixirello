use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :elixirello, ElixirelloWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :elixirello, Elixirello.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "elixirello_test",
  pool: Ecto.Adapters.SQL.Sandbox

config :elixirello, Elixirello.Repo,
  hostname: if(System.get_env("CI"), do: "postgres", else: "localhost")
