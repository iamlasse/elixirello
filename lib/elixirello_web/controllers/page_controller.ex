defmodule ElixirelloWeb.PageController do
  use ElixirelloWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
